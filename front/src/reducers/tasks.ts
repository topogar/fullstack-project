import { ITask } from '../types/task'
import {
  FETCH_TASKS_SUCCESS,
  FETCH_TASKS,
  FETCH_TASK_SUCCESS
} from '../actions/tasks'

const initialState: ITaskState = {
  taskList: [],
  tasks: {},
  isLoading: false
}

interface IAction {
  type: string
  payload: {
    result: number[] | number
    entities: { task: { [key: number]: ITask } }
  }
}

export default function(state = initialState, action: IAction) {
  switch (action.type) {
    case FETCH_TASKS: {
      return {
        ...state,
        isLoading: true
      }
    }
    case FETCH_TASK_SUCCESS:
      return {
        ...state,
        tasks: {
          ...state.tasks,
          ...action.payload.entities.task
        },
        taskList: [action.payload.result as number],
        isLoading: false
      }
    case FETCH_TASKS_SUCCESS:
      return {
        ...state,
        tasks: action.payload.entities.task,
        taskList: action.payload.result as number[],
        isLoading: false
      }
  }
  return state
}
