import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Board from './components/Board'
import Header from './components/Header'
import Task from './components/Task'
import { TaskCreate } from './components/TaskForm'
import Login from './components/Login'
import Registration from './components/Registration'
import initStore from './store'

const App: React.FC = () => {
  return (
    <Provider store={initStore()}>
      <Router>
        <div>
          <Header />
          <Route path="/" exact component={Board} />
          <Route path="/task/:taskId" component={Task} />
          <Route path="/task-create/" component={TaskCreate} />
          <Route path="/login" component={Login} />
          <Route path="/registration" component={Registration} />
        </div>
      </Router>
    </Provider>
  )
}

export default App
