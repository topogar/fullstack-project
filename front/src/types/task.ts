export interface ITask {
  name: string
  id: string
  description?: string
  image: string
}
