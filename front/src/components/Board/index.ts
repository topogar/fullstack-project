import { connect } from 'react-redux'
import { fetchTasks } from '../../actions/tasks'
import Board from './Board'

function mapStateToProps(state: IState) {
  return {
    taskList: state.tasks.taskList,
    isLoading: state.tasks.isLoading
  }
}

const mapDispatchToProps = {
  fetchTasks
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Board)
