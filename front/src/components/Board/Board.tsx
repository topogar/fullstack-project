import React from 'react'
import TaskCard from '../TaskCard'
import Spinner from '../../components/Spinner'
import * as S from './styled'

interface IBoardProps {
  fetchTasks: () => void
  taskList: number[]
  isLoading: boolean
}

const Board: React.FC<IBoardProps> = (props) => {
  const { fetchTasks, taskList, isLoading } = props

  React.useEffect(() => {
    fetchTasks()
  }, [])

  if (isLoading) {
    return <Spinner />
  }

  return (
    <S.BoardSection>
      {taskList.map((taskId, index) => (
        <TaskCard key={index} taskId={taskId} />
      ))}
    </S.BoardSection>
  )
}

export default Board
