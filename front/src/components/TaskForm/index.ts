import { connect } from 'react-redux'
import { taskCreate, taskUpdate } from '../../actions/tasks'

import TaskForm from './TaskForm'

const mapDispatchToPropsCreate = {
  taskSave: taskCreate
}

const mapDispatchToPropsUpdate = {
  taskSave: taskUpdate
}

export const TaskCreate = connect(
  null,
  mapDispatchToPropsCreate
)(TaskForm)

export const TaskUpdate = connect(
  null,
  mapDispatchToPropsUpdate
)(TaskForm)
