import React from 'react'
import { useDropzone } from 'react-dropzone'
import { FormWrapper, FormItem } from '../styled'
import * as S from './styled'

interface ITaskCreate {
  history?: {
    push: (url: string) => void
  }
  taskSave: (task: ITask) => void
  name?: string
  description?: string
  id?: number
  onSaveSuccess?: () => void
}

const TaskForm: React.FC<ITaskCreate> = (props) => {
  const { history, taskSave, id, onSaveSuccess } = props
  const [name, setName] = React.useState<string>(props.name || '')
  const [description, setDescription] = React.useState<string>(
    props.description || ''
  )

  const [image, setImage] = React.useState<File | null>(null)

  const onDrop = React.useCallback((acceptedFiles) => {
    if (acceptedFiles && acceptedFiles[0]) {
      setImage(acceptedFiles[0])
    }
  }, [])
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })

  const onSubmit = React.useCallback(
    async (event) => {
      event.preventDefault()
      taskSave({ name, description, id, image} as any)
      if (onSaveSuccess) {
        onSaveSuccess()
      }
      if (history) {
        history.push('')
      }
    },
    [name, description, image]
  )

  return (
    <FormWrapper>
      <form>
        <FormItem>
          <label htmlFor="name">Название теста</label>
          <input
            type="text"
            name="name"
            required
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
        </FormItem>
        <FormItem>
          <label htmlFor="description">Описание</label>
          <input
            type="text"
            name="description"
            value={description}
            onChange={(event) => setDescription(event.target.value)}
          />
        </FormItem>
        <FormItem>
          <label htmlFor="description">Картинка</label>
          <div {...getRootProps()}>
            <input {...getInputProps()} />
            <S.FileInput isDragActive={isDragActive}>
              {(image  && image.name) || 'Файл не выбран'}
            </S.FileInput>
          </div>
        </FormItem>
        <button type="submit" onClick={onSubmit}>
          Отправить
        </button>
      </form>
    </FormWrapper>
  )
}

export default TaskForm
