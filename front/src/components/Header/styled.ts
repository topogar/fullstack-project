import styled from 'styled-components'

export const butt = styled.div`
  cursor: pointer;
  border: 3px solid #218294;
  font-size: 20px;
  color: #218294;
  text-decoration: none;
  text-transform: uppercase;
  width: 200px;
  height: 40px;
  display: block;
  text-align: center;
  line-height: 40px;
  font-family: Arial, sans-serif;
  position: relative;
  transition: .5s;
  overflow: hidden;
&::before,
&::after {
  position: absolute;
  content: '';
  width: 100%;
  height: 100%;
  background: #218294;
  top: 0;
  left: -100%;
  opacity: .5;
  transition: .3s;
  z-index: -1;
 }
&::after {
  opacity: 1;
  transition-delay: .2s;
 }
&:hover {
  color: #fff;
 }
&:hover::before,
&:hover::after {
  left: 0;
 }
 `
export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 800px;
  margin: 0 auto;
  @media screen and (max-width: 800px) {
    flex-direction: column;
    align-items: center;

    a {
      max-width: 100%;
      margin: 5px;
    }

    ${butt} + ${butt} {
      margin-top: 5px;
    }
  }
  
`

export const LogoutButton = styled.a`
  cursor: pointer;
`
