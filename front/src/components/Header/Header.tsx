import React from 'react'
import { Link } from 'react-router-dom'
import getIsAuth from '../../utils/getIsAuth'
import * as S from './styled'

const Header: React.FC = () => {
  const onLogout = React.useCallback(() => {
    window.localStorage.clear()
    window.location.reload()
  }, [])
  
  return (
    <S.Header>
      <Link to="/">
        <S.butt>Примеры работы</S.butt>
      </Link>
      {getIsAuth() && (
        <Link to="/task-create">
          <S.butt>Загрузить</S.butt>
        </Link>
      )}
      <div>
        {getIsAuth() ? (
          <S.butt onClick={onLogout}>Выход</S.butt>
        ) : (
          <Link to="/login">
            <S.butt>Вход</S.butt>
          </Link>
        )}
      </div>
      {!getIsAuth() && (
        <Link to="/registration">
          <S.butt>Регистрация</S.butt>
        </Link>
      )}
    </S.Header>
  )
}

export default Header
