import React from 'react'
import { Link } from 'react-router-dom'
import image from '../../assets/image.jpg'
import * as S from './styled'

export interface ITaskStateCardProps {
  task: ITask
}

export interface ITaskOwnCardProps {
  taskId: number
}

interface ITaskCardProps
  extends ITaskStateCardProps,
    ITaskOwnCardProps {}

const TaskCard: React.FC<ITaskCardProps> = (props) => {
  const { task } = props

  if (!task) {
    return null
  }
  const { name, id, image } = task

  return (
    <Link to={`/task/${id}`}>
      <S.BoardItem>
        <img src={image} alt="Изображение" />
        <div>
          <S.Title>{name}</S.Title>
        </div>
      </S.BoardItem>
    </Link>
  )
}

export default TaskCard
