import { connect } from 'react-redux'
import TaskCard, {
  ITaskStateCardProps,
  ITaskOwnCardProps
} from './TaskCard'

function mapStateToProps(
  state: IState,
  props: ITaskOwnCardProps
): ITaskStateCardProps {
  const { taskId } = props

  return {
    task: state.tasks.tasks[taskId]
  }
}

export default connect(mapStateToProps)(TaskCard)
