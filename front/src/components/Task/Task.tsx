import React from 'react'
import iPhone from '../../assets/iphone.jpg'
import Spinner from '../Spinner'
import Modal from '../Modal'
import { TaskUpdate } from '../TaskForm'
import * as S from './styled'

export interface ITaskStateProps {
  isLoading: boolean
  task: ITask
}

export interface ITaskDispatchProps {
  fetchTask: (taskId: string) => void
  toggleModal: () => void
}

export interface ITaskOwnProps {
  match: {
    params: {
      taskId: string
    }
  }
}

interface ITaskProps
  extends ITaskStateProps,
    ITaskDispatchProps,
    ITaskOwnProps {}

const Task: React.FC<ITaskProps> = (props) => {
  const { fetchTask, match, isLoading, task, toggleModal } = props
  React.useEffect(() => {
    fetchTask(match.params.taskId)
  }, [])

  if (isLoading || !task) {
    return <Spinner />
  }

  const { name, description, image } = task

  return (
    <S.Wrapper>
      <S.Title>
        <h2>{name}</h2>
      </S.Title>
      <S.Img src={image} alt="..." />
      <S.Img src={image} alt="..." /> 
      <S.Desc>{description}</S.Desc>
      <S.butt onClick={() => toggleModal()}>Редактировать</S.butt>
      <Modal>
        <TaskUpdate
          name={name}
          description={description}
          id={Number(match.params.taskId)}
        />
      </Modal>
    </S.Wrapper>
  )
}

export default Task
