import { connect } from 'react-redux'
import { fetchTask } from '../../actions/tasks'
import { toggleModal } from '../../actions/modal'
import Task, {
  ITaskStateProps,
  ITaskDispatchProps,
  ITaskOwnProps
} from './Task'

function mapStateToProps(
  state: IState,
  props: ITaskOwnProps
): ITaskStateProps {
  return {
    task: state.tasks.tasks[Number(props.match.params.taskId)],
    isLoading: state.tasks.isLoading
  }
}

const mapDispatchToProps: ITaskDispatchProps = {
  fetchTask,
  toggleModal
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Task)
