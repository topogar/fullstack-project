import styled from 'styled-components'
import { Section } from '../styled'

export const Wrapper = styled(Section)`
  display: flex;
  flex-direction: column;
`

export const Title = styled.div`
  margin: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const Img = styled.img`
  width: 100%;
  border-radius: 8px;
`

export const Desc= styled.div`
  margin: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const butt = styled.div`
  cursor: pointer;
  border: 3px solid #218294;
  font-size: 20px;
  color: #218294;
  text-decoration: none;
  text-transform: uppercase;
  width: 800px;
  height: 40px;
  display: block;
  text-align: center;
  line-height: 40px;
  font-family: Arial, sans-serif;
  position: relative;
  transition: .5s;
  overflow: hidden;
&::before,
&::after {
  position: absolute;
  content: '';
  width: 100%;
  height: 100%;
  background: #218294;
  top: 0;
  left: -100%;
  opacity: .5;
  transition: .3s;
  z-index: -1;
 }
&::after {
  opacity: 1;
  transition-delay: .2s;
 }
&:hover {
  color: #fff;
 }
&:hover::before,
&:hover::after {
  left: 0;
 }
 `