import { Dispatch } from 'redux'
import { tasksNormalize, taskNormalize } from '../schemas/tasks'
import { toggleModal } from './modal'
import ApiClientService from '../services/ApiClientService'

export const FETCH_TASKS = 'FETCH_TASKS'
export const FETCH_TASKS_SUCCESS = 'FETCH_TASKS_SUCCESS'
export const FETCH_TASK_SUCCESS = 'FETCH_TASK_SUCCESS'

export function fetchTasks() {
  return async (dispatch: Dispatch) => {
    try {
      dispatch({
        type: FETCH_TASKS
      })

      const data = await ApiClientService('tasks/')

      if (data && Array.isArray(data)) {
        dispatch({
          type: FETCH_TASKS_SUCCESS,
          payload: tasksNormalize(data)
        })
      }
    } catch (err) {}
  }
}

export function fetchTask(taskId: string) {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: FETCH_TASKS
    })
    const data = await ApiClientService(`tasks/${taskId}/`)

    dispatch({
      type: FETCH_TASK_SUCCESS,
      payload: taskNormalize(data)
    })
  }
}

export function taskCreate({ name, description, image }: ITask) {
  return async () => {
    const formData = new FormData()

    formData.append('name', name)
    if (description) {
      formData.append('description', description)
    }
    if (image) {
      formData.append('image', image)
    }
    await ApiClientService('tasks/', {
      method: 'post',
      body: formData
    })
  }
}

export function taskUpdate({
  name,
  description,
  id,
  image
}: ITask) {
  return async (dispatch: Dispatch) => {
    const formData = new FormData()

    formData.append('name', name)
    if (description) {
      formData.append('description', description)
    }
    if (image) {
      formData.append('image', image)
    }
    const data = await ApiClientService(`mytasks/${id}/`, {
      method: 'put',
      body: formData
    })

    dispatch({
      type: FETCH_TASK_SUCCESS,
      payload: taskNormalize(data)
    })

    dispatch(toggleModal())
  }
}