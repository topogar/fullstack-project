import { ITask } from '../types/task'
import { normalize, schema } from 'normalizr'

const taskSchema = new schema.Entity('task')

export function tasksNormalize(tasks: ITask[]) {
  return normalize(tasks, [taskSchema])
}

export function taskNormalize(tasks: ITask) {
  return normalize(tasks, taskSchema)
}
