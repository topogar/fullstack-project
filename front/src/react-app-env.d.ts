/// <reference types="react-scripts" />

interface ITask {
  name: string
  id: string
  description?: string
  image: string
}

interface ITaskState {
  isLoading: boolean
  taskList: number[]
  tasks: { [key: number]: ITask }
}

interface IModalState {
  isModalOpen: boolean
}

interface IState {
  tasks: ITaskState
  modal: IModalState
}
