from django.db import models
from django.contrib.auth.models import User
from task.models import Task

class Comment(models.Model):
  text = models.TextField()
  author = models.ForeignKey(User, on_delete=models.CASCADE)
  task = models.ForeignKey(Task, on_delete=models.CASCADE)

  def __str__(self):
      return self.text
  
